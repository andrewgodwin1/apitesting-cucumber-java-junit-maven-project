# Behaviour driven Test automation framework

:star: Star me on GitHub/Github — it helps. Thanks!  

This project is a black-box restful service test, testing a story that involves talking to three different resources that require different sets of input. The responses are used to fill three dropdowns, each dependent on the input of the one before.  

The Story is: As a customer, I want to select the type of car I want to sell, so that I can receive an offer from ACompany.  

The test is written in java using cucumber, Junit, Maven, Postman for discovery/exploration and Rest Assured for API calls to API Server.  


## Steps to execute the tests

### Method 1
1. from any terminal or command line prompt, cd into the project directory
2. mvn clean test

### Method 2
1. import project into your favorite IDE
2. sync maven and run the test from the TestRunner class

Voila!

### Authors
1. Andrew Godwin

### Internet Presence
Website: https://www.andrew-godwin.com  
Github Profile: https://github.com/pasignature    
Gitlab Profile: https://gitlab.com/pasignature  
Linkedin Profile: https://www.linkedin.com/in/pasignature/  
StackOverflow Profile: https://stackoverflow.com/users/5160455/pasignature

