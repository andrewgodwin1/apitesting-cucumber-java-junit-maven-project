package helperServices;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class APIClient {
	
	protected String base_url;
	protected String wa_key;
	
	/**
	 * Constructor initializes the object of APIClient class
	 * It prepares the new object for use accepting base_url and wa_key
	 * as arguments which the constructor uses to set member variables
	 *
	 * @param base_url - base API URL for all API calls
	 * @param wa_key - API key for all API calls
	 */
	public APIClient(String base_url, String wa_key) {
		this.setBase_url(base_url);
		this.setWa_key(wa_key);
	}

	public String getBase_url() {
		return base_url;
	}

	public void setBase_url(String base_url) {
		this.base_url = base_url;
	}
	
	public String getWa_key() {
		return wa_key;
	}

	public void setWa_key(String wa_key) {
		this.wa_key = wa_key;
	}
	
	
	/**
	 * Constructs and formats request URL
	 *
	 * @param resourcePath - resource path location
	 * @param locale - any locale
	 * @return String
	 */
	public String BuildURL(String resourcePath, String locale){
		
		String uri = null;
		try {
			uri = new URIBuilder(getBase_url())
			        .setPath(resourcePath)
			        .setParameter("wa_key", getWa_key())
			        .setParameter("locale", locale)
			        .build().toURL().toString();
	
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return  uri;
	}
	
	
	/**
	 * Requests data from API server using RestAssured API
	 *
	 * @param requestURL - API request URL
	 * @return String
	 */
	public String sendRequest(String requestURL){
		
		// send request to API server
		Response response = RestAssured.get(requestURL);
		
		// store the response body as string
		String responseBody = response.getBody().asString();
		
		// store the response code
		//int responseStatusCode = response.getStatusCode();
		
		//finally
		return responseBody;
	}
	
	
}
