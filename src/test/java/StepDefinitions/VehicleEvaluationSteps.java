package StepDefinitions;

import static org.junit.Assert.assertTrue;

import helperServices.APIClient;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VehicleEvaluationSteps {
	
	protected static final String BASE_URL = "https://api-aws-eu-qa-1.auto1-test.com";
	protected static final String WA_KEY = "coding-puzzle-client-449cc9d";
	private static String requestURL;
	private static String responseBody;
	private static String manufacturerCode;
	private static String mainTypeCode;

	@Given("^manufacturer (.*) path and (.*)$")
	public void webservices_manufacturer_path_and_en(String manufacturerPath, String locale) {
	    
	    //setting request URL
	    APIClient url = new APIClient(BASE_URL, WA_KEY);
	    requestURL = url.BuildURL(manufacturerPath, locale);
	}

	@When("customer requests a list of manufacturers")
	public void customer_requests_a_list_of_manufacturers() {
		
		// sending request to server with resource parameters
		APIClient request = new APIClient(null, null);
		responseBody = request.sendRequest(requestURL);
	}

	@Then("^response body should contain (.*)$")
	public void response_body_should_contain(String wkda) {
		
		assertTrue(responseBody.contains(wkda));
	}

	@Given("list of manufacturer options")
	public void list_of_manufacturer_options() {
		//System.out.println(responseBody);
	}

	@When("customer selects a manufacturer")
	public void customer_selects_a_manufacturer() {
		//System.out.println(responseBody);
	}

	@Then("^chosen (.*) should be displayed$")
	public void chosen_manufacturer_should_be_displayed(String manufacturer) {
		manufacturerCode = manufacturer;
		assertTrue(responseBody.contains(manufacturerCode));
	}

	@Given("^main-types (.*) path and (.*)$")
	public void webservices_main_types_path_and_en(String mainTypePath, String locale) {
		
		//setting request URL
	    APIClient url = new APIClient(BASE_URL, WA_KEY);
	    requestURL = url.BuildURL(mainTypePath, locale);
	}

	@When("request is sent to server with manufacturer code")
	public void request_is_sent_to_server_with_manufacturer_code() {
		
		// sending request with resource parameters and query string
		APIClient request = new APIClient(null, null);
		   responseBody = request.sendRequest(requestURL + 
				   "&manufacturer=" + manufacturerCode);
	}

	@Given("list of main type options")
	public void list_of_main_type_options() {
		//System.out.println(responseBody);
	}

	@When("customer selects a main type")
	public void customer_selects_a_main_type() {
		//System.out.println(responseBody);
	}

	@Then("^selected (.*) should be displayed$")
	public void chosen_mainType_should_be_displayed(String mainType) {
		mainTypeCode = mainType;
		assertTrue(responseBody.contains(mainTypeCode));
	}

	@Given("^built-dates (.*) path and (.*)$")
	public void webservices_built_dates_path_and_en(String builtDatePath, String locale) {
		
		//setting request URL
	    APIClient url = new APIClient(BASE_URL, WA_KEY);
	    requestURL = url.BuildURL(builtDatePath, locale);
	}

	@When("request is sent to server with manufacturer code and maintype code")
	public void request_is_sent_to_server_with_manufacturer_code_and_maintype_code() {
		
		// sending request with resource parameters and query string
		APIClient request = new APIClient(null, null);
		   responseBody = request.sendRequest(requestURL + 
				   "&manufacturer=" + manufacturerCode + "&main-type=" + mainTypeCode);
	}

	@Given("list of built date options")
	public void list_of_built_date_options() {
		//System.out.println(responseBody);
	}

	@When("customer selects a built date")
	public void customer_selects_a_built_date() {
		//System.out.println(responseBody);
	}

	@Then("^required (.*) should be displayed$")
	public void built_date_should_be_displayed(String builtDate) {
		assertTrue(responseBody.contains(builtDate));
	}
}
