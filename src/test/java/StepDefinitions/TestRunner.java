package StepDefinitions;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features",
glue={"StepDefinitions"},
monochrome=true,
dryRun=false,
plugin= {"pretty",
		"junit:target/JunitReports/cucumber.xml",
		"json:target/JsonReports/cucumber.json"})

public class TestRunner{

}
