@JIRA-Story-0012
Feature: Free Vehicle Evaluation Request
  To receive an offer from ACompany, customer needs
  to select type of car he needs to sell

  Scenario Outline: List and select manufacturer brands
    Given manufacturer <resource> path and <locale>
    When customer requests a list of manufacturers
    Then response body should contain "wkda"
    Given list of manufacturer options
    When customer selects a manufacturer
    Then chosen <manufacturer> should be displayed

    Examples: 
      | resource                   | locale | manufacturer |
      | /v1/car-types/manufacturer | EN     |          853 |

  Scenario Outline: List and select main-types
    Given main-types <resource> path and <locale>
    When request is sent to server with manufacturer code
    Then response body should contain "wkda"
    Given list of main type options
    When customer selects a main type
    Then selected <main type> should be displayed

    Examples: 
      | resource                 | locale | main type |
      | /v1/car-types/main-types | EN     | Roadster  |

  Scenario Outline: List and select built-dates
    Given built-dates <resource> path and <locale>
    When request is sent to server with manufacturer code and maintype code
    Then response body should contain "wkda"
    Given list of built date options
    When customer selects a built date
    Then required <built date> should be displayed

    Examples: 
      | resource                  | locale | built date |
      | /v1/car-types/built-dates | EN     |       2014 |
